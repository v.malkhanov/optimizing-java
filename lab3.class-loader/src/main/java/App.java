import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class App {
    private static void usage() {
        System.out.println("First argument must be path dir to classes");
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            usage();
            return;
        }

        Path dir = Paths.get(args[0]);
        if (!Files.isDirectory(dir)) {
            usage();
            return;
        }
        try (Stream<Path> paths = Files.walk(dir)) {
            CustomClassLoader classLoader = new CustomClassLoader();
            paths.filter(Files::isRegularFile)
                    .filter(path -> path.toString().endsWith(".class"))
                    .forEach(path -> CustomClassLoader.execute(path, classLoader));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
