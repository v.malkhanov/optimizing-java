public class AsmUtils {
    public static String absoluteName(Class<?> in) {
        return in.getCanonicalName().replaceAll("[.]", "/");
    }

    public static String asmName(Class<?> in) {
        return 'L' + AsmUtils.absoluteName(in) + ';';
    }
}
