package constant;

public class DynamicInfo extends ClassFileCpInfo {
    private final short bootstrapMethodAttrIndex;
    private final short nameAndTypeIndex;

    public DynamicInfo(final Tag tag, final short bootstrapMethodAttrIndex, final short nameAndTypeIndex) {
        super(tag);
        this.bootstrapMethodAttrIndex = bootstrapMethodAttrIndex;
        this.nameAndTypeIndex = nameAndTypeIndex;
    }

    @Override
    public String getInfo() {
        return String.format("#%d:#%d", bootstrapMethodAttrIndex, nameAndTypeIndex);
    }

    @Override
    public String getNormalizeInfo(ClassFileCpInfo[] constantPool) {
        if (nameAndTypeIndex - 1 < constantPool.length) {
            return String.format("#%d:%s", bootstrapMethodAttrIndex,
                    constantPool[nameAndTypeIndex - 1].getNormalizeInfo(constantPool));
        }
        return this.getInfo();
    }
}
