package constant;

import java.util.Arrays;

public enum MethodHandle {
    REF_GET_FIELD((byte) 1),
    REF_GET_STATIC((byte) 2),
    REF_PUT_FIELD((byte) 3),
    REF_PUT_STATIC((byte) 4),
    REF_INVOKE_VIRTUAL((byte) 5),
    REF_INVOKE_STATIC((byte) 6),
    REF_INVOKE_SPECIAL((byte) 7),
    REF_NEW_INVOKE_SPECIAL((byte) 8),
    REF_INVOKE_INTERFACE((byte) 9);

    private final byte kind;

    MethodHandle(final byte kind) {
        this.kind = kind;
    }

    public byte getKind() {
        return kind;
    }

    public static MethodHandle valueOf(final byte kind) {
        return Arrays.stream(MethodHandle.values())
                .filter(it -> it.kind == kind)
                .findAny()
                .orElse(null);
    }
}
