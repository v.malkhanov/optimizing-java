public class App {
    public static void main(String[] args) {
        SpinLock spinLock = new SpinLock();

        Thread first = new Thread(() -> {
            spinLock.lock(1000);
            System.out.println("FIRST START");
            try {
                Thread.sleep(1500);
            } catch (InterruptedException ignored) {
            }
            System.out.println("FIRST END");
            spinLock.unlock();
        });

        Thread second = new Thread(() -> {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignored) {
            }
            spinLock.lock(1000);
            System.out.println("SECOND START");
            System.out.println("SECOND END");
            spinLock.unlock();
        });

        first.start();
        second.start();
    }
}
