package constant;

public class NameAndTypeInfo extends ClassFileCpInfo {
    private final short nameIndex;
    private final short descriptorIndex;

    public NameAndTypeInfo(Tag tag, short nameIndex, short descriptorIndex) {
        super(tag);
        this.nameIndex = nameIndex;
        this.descriptorIndex = descriptorIndex;
    }

    @Override
    public String getInfo() {
        return String.format("#%d:#%d", nameIndex, descriptorIndex);
    }

    @Override
    public String getNormalizeInfo(ClassFileCpInfo[] constantPool) {
        if (nameIndex - 1 < constantPool.length && descriptorIndex - 1 < constantPool.length) {
            return String.format("%s:%s",
                    constantPool[nameIndex - 1].getNormalizeInfo(constantPool),
                    constantPool[descriptorIndex - 1].getNormalizeInfo(constantPool));
        }
        return this.getInfo();
    }
}
