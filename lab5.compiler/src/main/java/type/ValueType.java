package type;

public enum ValueType {
    STRING,
    INTEGER,
    VAR_NAME
}
