package constant;

public class InfoWithIndex extends ClassFileCpInfo {
    private final short index;

    public InfoWithIndex(final Tag tag, final short index) {
        super(tag);
        this.index = index;
    }

    @Override
    public String getInfo() {
        return String.format("#%d", index);
    }

    @Override
    public String getNormalizeInfo(ClassFileCpInfo[] constantPool) {
        if (index - 1 < constantPool.length) {
            return constantPool[index - 1].getNormalizeInfo(constantPool);
        }
        return this.getInfo();
    }
}
