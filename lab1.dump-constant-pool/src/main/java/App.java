import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class App {
    private static void usage(String[] args) {
        System.err.println("Usage: ./dump_constant_pool <class file>");
        System.err.println("Expected: <class file>");
        System.err.println("Actual: " + Arrays.toString(args));
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            usage(args);
            return;
        }
        String fileName = args[0];
        try (InputStream in = new FileInputStream(fileName)) {
            ClassFileReader reader = new ClassFileReader(in);
            System.out.println(reader.toString());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}