import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import java.io.File;
import java.io.IOException;

public class App {
    public static void main(String[] args) {
        if (args.length < 1) {
            return;
        }
        String fileName = args[0];
        try {
            File file = new File(fileName);
            Source source = Source.newBuilder("llvm", file).build();
            Context context = Context.newBuilder().allowAllAccess(true).build();
            Value lib = context.eval(source);
            Value getCpuInfo = lib.getMember("getcpuinfo");
            getCpuInfo.executeVoid();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
