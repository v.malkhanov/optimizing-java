import net.bytebuddy.agent.ByteBuddyAgent;
import net.bytebuddy.asm.Advice;

public class MainAdvice {

    @Advice.OnMethodExit(onThrowable = Throwable.class)
    static void exit() {
        System.out.println("Min Time: " + TimerAdvice.minTime() + "ms");
        System.out.println("Max Time: " + TimerAdvice.maxTime() + "ms");
        System.out.println("Average Time: " + TimerAdvice.averageTime() + "ms");
        System.out.println("Number of loaded classes: " + ByteBuddyAgent.getInstrumentation().getAllLoadedClasses().length);
    }

}
