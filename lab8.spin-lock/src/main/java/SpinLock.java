import java.util.concurrent.atomic.AtomicBoolean;

public class SpinLock {
    private final AtomicBoolean lock = new AtomicBoolean(false);

    // milliseconds
    public void lock(long timeout) {
        while (!lock.compareAndSet(false, true)) {
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException ignored) {}
        }
    }

    public void unlock() {
        lock.set(false);
    }
}
