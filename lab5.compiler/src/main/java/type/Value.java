package type;

public class Value {
    public ValueType type;
    public Object value;

    public Value(ValueType type, Object value) {
        this.type = type;
        this.value = value;
    }
}
