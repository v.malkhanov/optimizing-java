package main;

import java.util.regex.Pattern;

public class Number {

    private static final Pattern pattern = Pattern.compile("\\d+");

    public static boolean isNumberParseInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public static boolean isNumberForCharacter(String str) {
        return str.chars().allMatch(Character::isDigit);
    }

    public static boolean isNumberRegexp(String str) {
        return pattern.matcher(str).matches();
    }

}
