package type;

public class IfStatement {
    private static int ID_COUNTER = 0;

    public final int id;
    public final Value left;
    public final Value right;
    public final String condition;
    public final boolean isNot;

    public IfStatement(Value right, String condition, Value left, boolean isNot) {
        this.id = ID_COUNTER++;
        this.left = left;
        this.right = right;
        this.condition = condition;
        this.isNot = isNot;
    }
}

