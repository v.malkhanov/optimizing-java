import net.bytebuddy.asm.Advice;

import java.util.ArrayList;
import java.util.List;

import static net.bytebuddy.implementation.bytecode.assign.Assigner.Typing.DYNAMIC;

public class TimerAdvice {
    public static final List<Long> timers = new ArrayList<>();

    @Advice.OnMethodEnter
    static long enter(@Advice.Argument(value = 0, typing = DYNAMIC, readOnly = false) Object txNum) {
        txNum = (Integer) txNum + 99;
        return System.currentTimeMillis();
    }

    @Advice.OnMethodExit(onThrowable = Throwable.class)
    static void exit(@Advice.Enter long start) {
        timers.add(System.currentTimeMillis() - start);
    }

    public static Long maxTime() {
        return timers.stream().mapToLong(Long::longValue).max().orElse(0L);
    }

    public static Long minTime() {
        return timers.stream().mapToLong(Long::longValue).min().orElse(0L);
    }

    public static Double averageTime() {
        return timers.stream().mapToLong(Long::longValue).average().orElse(Double.NaN);
    }
}
