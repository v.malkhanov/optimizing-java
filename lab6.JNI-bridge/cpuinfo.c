#include <stdio.h>
#include <unistd.h>

#define CHUNK 1024

void getcpuinfo() {
    char buf[CHUNK];
    FILE * file = fopen("/proc/cpuinfo", "r");;
    if (file) {
        size_t nread;
        while ((nread = fread(buf, sizeof(char), CHUNK, file)) > 0) {
            fwrite(buf, 1, nread, stdout);
        }
        fclose(file);
    }
}

int main() {
    getcpuinfo();
    return 0;
}