import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import type.IfStatement;
import type.Value;
import type.ValueType;
import type.Var;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class Program implements Opcodes, AutoCloseable {

    private final ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
    private final MethodVisitor main;

    private final Map<String, Var> vars = new HashMap<>();
    private final Map<String, Label> labels = new HashMap<>();
    private final String className;

    public Program(Path file) {
        this.className = file.getFileName().toString().split("[.]")[0];
        classWriter.visit(V11, ACC_PUBLIC + ACC_SUPER, className,
            null, AsmUtils.absoluteName(Object.class), null);

        MethodVisitor constructor = classWriter.visitMethod(ACC_PUBLIC, "<init>",
            "()V", null, null);
        constructor.visitCode();
        constructor.visitVarInsn(ALOAD, 0);
        constructor.visitMethodInsn(INVOKESPECIAL, AsmUtils.absoluteName(Object.class), "<init>", "()V", false);
        constructor.visitInsn(RETURN);
        constructor.visitMaxs(1, 1);
        constructor.visitEnd();

        main = classWriter.visitMethod(ACC_PUBLIC + ACC_STATIC, "main",
            String.format("([%s)V", AsmUtils.asmName(String.class)), null, null);
        main.visitCode();
    }

    public Program assignment(Value value, String name) {
        switch (value.type) {
            case INTEGER: {
                Var var = getOrCreateVar(name, ValueType.INTEGER);
                main.visitLdcInsn(value.value);
                main.visitVarInsn(ISTORE, var.id);
                break;
            }
            case STRING: {
                Var var = getOrCreateVar(name, ValueType.STRING);
                main.visitLdcInsn(value.value);
                main.visitVarInsn(ASTORE, var.id);
                break;
            }
            case VAR_NAME: {
                Var from = vars.get((String) value.value);
                if (from == null) {
                    throw new IllegalArgumentException(String.format("Not exist var with name %s", value.value));
                }
                Var to = getOrCreateVar(name, from.type);
                switch (from.type) {
                    case INTEGER:
                        main.visitVarInsn(ILOAD, from.id);
                        main.visitVarInsn(ISTORE, to.id);
                        break;
                    case STRING:
                        main.visitVarInsn(ALOAD, from.id);
                        main.visitVarInsn(ASTORE, to.id);
                }
            }
        }
        return this;
    }

    public Program println(String name) {
        Var var = vars.get(name);
        if (var == null) {
            throw new IllegalArgumentException(String.format("Not exist var with name %s", name));
        }
        main.visitFieldInsn(GETSTATIC, AsmUtils.absoluteName(System.class), "out", AsmUtils.asmName(PrintStream.class));
        switch (var.type) {
            case STRING:
                main.visitVarInsn(ALOAD, var.id);
                main.visitMethodInsn(INVOKEVIRTUAL, AsmUtils.absoluteName(PrintStream.class),
                    "println", String.format("(%s)V", AsmUtils.asmName(String.class)), false);
                break;
            case INTEGER:
                main.visitVarInsn(ILOAD, var.id);
                main.visitMethodInsn(INVOKEVIRTUAL, AsmUtils.absoluteName(PrintStream.class),
                    "println", "(I)V", false);
        }
        return this;
    }

    public Program label(String labelName) {
        Label label = getOrCreateLabel(labelName);
        main.visitLabel(label);
        return this;
    }

    public Program gotoLabel(String labelName) {
        Label label = getOrCreateLabel(labelName);
        main.visitJumpInsn(GOTO, label);
        return this;
    }

    public Integer ifBegin(IfStatement ifStatement) {
        ValueType left = load(ifStatement.left);
        ValueType right = load(ifStatement.right);
        if (left != right) {
            throw new IllegalArgumentException("Incomparable types");
        }
        Label label = getOrCreateLabel(String.valueOf(ifStatement.id));
        if (left == ValueType.STRING) {
            main.visitMethodInsn(INVOKEVIRTUAL, AsmUtils.absoluteName(String.class),
                "compareTo", String.format("(%s)I", AsmUtils.asmName(String.class)), false);
            main.visitInsn(ICONST_0);
        }
        switch (ifStatement.condition) {
            case ">":
                if (ifStatement.isNot) {
                    main.visitJumpInsn(IF_ICMPGT, label);
                } else {
                    main.visitJumpInsn(IF_ICMPLE, label);
                }
                break;
            case "<":
                if (ifStatement.isNot) {
                    main.visitJumpInsn(IF_ICMPLT, label);
                } else {
                    main.visitJumpInsn(IF_ICMPGE, label);
                }
                break;
            case "==":
                if (ifStatement.isNot) {
                    main.visitJumpInsn(IF_ICMPEQ, label);
                } else {
                    main.visitJumpInsn(IF_ICMPNE, label);
                }
        }
        return ifStatement.id;
    }

    public Program ifEnd(int ifId) {
        Label label = labels.get(String.valueOf(ifId));
        if (label == null) {
            throw new IllegalArgumentException(String.format("Not exist label with name %d", ifId));
        }
        main.visitLabel(label);
        return this;
    }

    private ValueType load(Value value) {
        switch (value.type) {
            case INTEGER:
            case STRING:
                main.visitLdcInsn(value.value);
                return value.type;
            case VAR_NAME:
                Var var = vars.get((String) value.value);
                if (var == null) {
                    throw new IllegalArgumentException(String.format("Not exist var with name %s", value.value));
                }
                switch (var.type) {
                    case INTEGER:
                        main.visitVarInsn(ILOAD, var.id);
                        break;
                    case STRING:
                        main.visitVarInsn(ALOAD, var.id);
                }
                return var.type;
        }
        return ValueType.VAR_NAME;
    }

    private Var getOrCreateVar(String name, ValueType type) {
        Var var = vars.get(name);
        if (var == null) {
            var = new Var(type);
            vars.put(name, var);
        }
        return var;
    }

    private Label getOrCreateLabel(String name) {
        Label label = labels.get(name);
        if (label == null) {
            label = new Label();
            labels.put(name, label);
        }
        return label;
    }

    @Override
    public void close() {
        main.visitInsn(RETURN);
        main.visitMaxs(0, 0);
        main.visitEnd();

        classWriter.visitEnd();
        try (FileOutputStream out = new FileOutputStream(className + ".class")) {
            out.write(classWriter.toByteArray());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
