import constant.ClassFileCpInfo;
import constant.Tag;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ClassFileReader {
    private static final int MAGIC = 0xCAFEBABE;

    private final ClassFileVersion version;
    private final short constantPoolCount;
    private final ClassFileCpInfo[] constantPool;

    public ClassFileReader(final InputStream input) throws IllegalArgumentException, IOException {
        try (DataInputStream in = new DataInputStream(input)) {
            if (in.readInt() != MAGIC) {
                throw new IllegalArgumentException("This is not a class file");
            }
            this.version = new ClassFileVersion(in.readShort(), in.readShort());
            this.constantPoolCount = in.readShort();
            this.constantPool = new ClassFileCpInfo[constantPoolCount - 1];
            for (int i = 0; i < constantPoolCount - 1; ++i) {
                ClassFileCpInfo cpInfo = ClassFileCpInfo.create(in.readByte(), in);
                this.constantPool[i] = cpInfo;
                if (Tag.isLongOrDouble(cpInfo.getTag())) {
                    ++i;
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder(String.format("Class file format version: %d.%d\n",
                version.getMajorVersion(), version.getMinorVersion()));
        for (int i = 1; i < constantPoolCount; ++i) {
            ClassFileCpInfo it = constantPool[i - 1];
            if (it == null) continue;
            res.append(String.format("%5s = %-24s %s\n", "#" + i, it.getTag(), it.getFullInfo(constantPool)));
        }
        return res.toString();
    }
}
