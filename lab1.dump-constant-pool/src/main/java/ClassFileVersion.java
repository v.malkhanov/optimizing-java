public class ClassFileVersion {
    private final short minorVersion;
    private final short majorVersion;

    public ClassFileVersion(final short minorVersion, final short majorVersion) {
        this.minorVersion = minorVersion;
        this.majorVersion = majorVersion;
    }

    public short getMajorVersion() {
        return majorVersion;
    }

    public short getMinorVersion() {
        return minorVersion;
    }
}
