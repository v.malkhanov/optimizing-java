package constant;

public class MethodHandleInfo extends ClassFileCpInfo {
    private final MethodHandle referenceKind;
    private final short referenceIndex;

    public MethodHandleInfo(Tag tag, byte referenceKind, short referenceIndex) {
        super(tag);
        this.referenceKind = MethodHandle.valueOf(referenceKind);
        this.referenceIndex = referenceIndex;
    }

    @Override
    public String getInfo() {
        return String.format("%d:#%d", referenceKind.getKind(), referenceIndex);
    }

    @Override
    public String getNormalizeInfo(ClassFileCpInfo[] constantPool) {
        if (referenceIndex - 1 < constantPool.length) {
            return String.format("%s %s", referenceKind,
                    constantPool[referenceIndex - 1].getNormalizeInfo(constantPool));
        }
        return this.getInfo();
    }
}
