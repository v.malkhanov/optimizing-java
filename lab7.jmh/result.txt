Benchmark                             (test)  Mode  Cnt   Score    Error  Units
NumberBenchmark.characterIsDigit  1234567890  avgt   25  ≈ 10⁻⁴           ms/op
NumberBenchmark.characterIsDigit  a123456789  avgt   25  ≈ 10⁻⁴           ms/op
NumberBenchmark.characterIsDigit  123456789a  avgt   25  ≈ 10⁻⁴           ms/op
NumberBenchmark.characterIsDigit  1234aa7890  avgt   25  ≈ 10⁻⁴           ms/op
NumberBenchmark.parseInt          1234567890  avgt   25  ≈ 10⁻⁴           ms/op
NumberBenchmark.parseInt          a123456789  avgt   25   0,003 ±  0,001  ms/op
NumberBenchmark.parseInt          123456789a  avgt   25   0,003 ±  0,001  ms/op
NumberBenchmark.parseInt          1234aa7890  avgt   25   0,003 ±  0,001  ms/op
NumberBenchmark.regexp            1234567890  avgt   25  ≈ 10⁻⁴           ms/op
NumberBenchmark.regexp            a123456789  avgt   25  ≈ 10⁻⁴           ms/op
NumberBenchmark.regexp            123456789a  avgt   25  ≈ 10⁻⁴           ms/op
NumberBenchmark.regexp            1234aa7890  avgt   25  ≈ 10⁻⁴           ms/op