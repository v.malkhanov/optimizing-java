package constant;

public class RefInfo extends ClassFileCpInfo {
    private final short classIndex;
    private final short nameAndTypeIndex;

    public RefInfo(final Tag tag, final short classIndex, final short nameAndTypeIndex) {
        super(tag);
        this.classIndex = classIndex;
        this.nameAndTypeIndex = nameAndTypeIndex;
    }

    @Override
    public String getInfo() {
        return String.format("#%d.#%d", classIndex, nameAndTypeIndex);
    }

    @Override
    public String getNormalizeInfo(ClassFileCpInfo[] constantPool) {
        if (classIndex - 1 < constantPool.length && nameAndTypeIndex - 1 < constantPool.length) {
            return String.format("%s.%s",
                    constantPool[classIndex - 1].getNormalizeInfo(constantPool),
                    constantPool[nameAndTypeIndex - 1].getNormalizeInfo(constantPool));
        }
        return this.getInfo();
    }
}
