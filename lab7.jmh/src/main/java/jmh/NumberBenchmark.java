package jmh;

import main.Number;
import org.openjdk.jmh.Main;
import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Warmup(iterations = 3)
@Measurement(iterations = 5)
@Threads(Threads.MAX)
public class NumberBenchmark {

    @Param(value = {"1234567890", "a123456789", "123456789a", "1234aa7890"})
    public String test;

    @Benchmark
    public boolean regexp(NumberBenchmark in) {
        return Number.isNumberRegexp(in.test);
    }

    @Benchmark
    public boolean parseInt(NumberBenchmark in) {
        return Number.isNumberParseInt(in.test);
    }

    @Benchmark
    public boolean characterIsDigit(NumberBenchmark in) {
        return Number.isNumberForCharacter(in.test);
    }

    public static void main(String[] args) {
        try {
            Main.main(args);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
