package constant;

import java.util.Arrays;

public enum Tag {
    UTF_8((byte) 1),
    INTEGER((byte) 3),
    FLOAT((byte) 4),
    LONG((byte) 5),
    DOUBLE((byte) 6),
    CLASS((byte) 7),
    STRING((byte) 8),
    FIELD_REF((byte) 9),
    METHOD_REF((byte) 10),
    INTERFACE_METHOD_REF((byte) 11),
    NAME_AND_TYPE((byte) 12),
    METHOD_HANDLE((byte) 15),
    METHOD_TYPE((byte) 16),
    DYNAMIC((byte) 17),
    INVOKE_DYNAMIC((byte) 18),
    MODULE((byte) 19),
    PACKAGE((byte) 20);

    private final byte tag;

    Tag(final byte tag) {
        this.tag = tag;
    }

    public static Tag valueOf(final byte tag) {
        return Arrays.stream(Tag.values())
                .filter(it -> it.tag == tag)
                .findAny()
                .orElse(null);
    }

    public static boolean isLongOrDouble(final Tag tag) {
        return tag == Tag.LONG || tag == Tag.DOUBLE;
    }
}
