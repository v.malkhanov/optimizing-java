package constant;

import java.io.DataInputStream;
import java.io.IOException;

public abstract class ClassFileCpInfo {
    private final Tag tag;

    protected ClassFileCpInfo(final Tag tag) {
        this.tag = tag;
    }

    protected abstract String getInfo();

    protected String getNormalizeInfo(final ClassFileCpInfo[] constantPool) {
        return this.getInfo();
    }

    public String getFullInfo(final ClassFileCpInfo[] constantPool) {
        if (this instanceof ValueInfo) {
            return String.format("%s", this.getNormalizeInfo(constantPool));
        }
        return String.format("%-14s // %s", this.getInfo(), this.getNormalizeInfo(constantPool));
    }

    public Tag getTag() {
        return tag;
    }

    public static ClassFileCpInfo create(final byte byteTag, final DataInputStream in) throws IOException {
        Tag tag = Tag.valueOf(byteTag);
        switch (tag) {
            case INTEGER:
                return new ValueInfo<>(tag, in.readInt());
            case FLOAT:
                return new ValueInfo<>(tag, in.readFloat());
            case LONG:
                return new ValueInfo<>(tag, in.readLong());
            case DOUBLE:
                return new ValueInfo<>(tag, in.readDouble());
            case UTF_8:
                short len = in.readShort();
                return new ValueInfo<>(tag, new String(in.readNBytes(len)));

            case PACKAGE:
            case MODULE:
            case CLASS:
            case METHOD_TYPE:
            case STRING:
                return new InfoWithIndex(tag, in.readShort());

            case DYNAMIC:
            case INVOKE_DYNAMIC:
                return new DynamicInfo(tag, in.readShort(), in.readShort());

            case FIELD_REF:
            case METHOD_REF:
            case INTERFACE_METHOD_REF:
                return new RefInfo(tag, in.readShort(), in.readShort());

            case METHOD_HANDLE:
                return new MethodHandleInfo(tag, in.readByte(), in.readShort());

            case NAME_AND_TYPE:
                return new NameAndTypeInfo(tag, in.readShort(), in.readShort());
            default:
                throw new IllegalArgumentException("Unexpected tag");
        }
    }
}
