import java.io.FileOutputStream;
import java.io.IOException;

public class App {
    public static void main(String[] args) {
        try (FileOutputStream out = new FileOutputStream("GuessNumber.class")) {
            GuessNumberGame game = new GuessNumberGame();
            out.write(game.generate());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
