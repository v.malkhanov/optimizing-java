import org.parboiled.BaseParser;
import org.parboiled.Rule;
import org.parboiled.annotations.BuildParseTree;
import type.IfStatement;
import type.Value;
import type.ValueType;

@BuildParseTree
public class Parser extends BaseParser<Object> {

    public Program program;

    public Rule Program() {
        return OneOrMore(
            FirstOf(
                WhiteSpace(),
                Goto(),
                If(),
                Println(),
                Label(),
                Assignment()
            )
        );
    }

    public Rule If() {
        return Sequence(
            String("if"),
            FirstOf(
                Sequence(WhiteSpace(), String("not"), push(true)),
                push(false)
            ),
            Optional(WhiteSpace()),
            Ch('('),
            Optional(WhiteSpace()),
            Expression(),
            Optional(WhiteSpace()),
            Ch(')'),
            push(program.ifBegin(new IfStatement((Value) pop(), (String) pop(), (Value) pop(), (boolean) pop()))),
            Optional(WhiteSpace()),
            Ch('{'),
            Optional(WhiteSpace()),
            Program(),
            Optional(WhiteSpace()),
            Ch('}'),
            push(program.ifEnd((Integer) pop())),
            drop()
        );
    }

    public Rule Expression() {
        return Sequence(
            FirstOf(IntegerLiteral(), StringLiteral(), VarName()),
            Optional(WhiteSpace()),
            Sequence(FirstOf(">", "<", "=="), push(match())),
            Optional(WhiteSpace()),
            FirstOf(IntegerLiteral(), StringLiteral(), VarName())
        );
    }

    public Rule Goto() {
        return Sequence(
            String("goto"),
            WhiteSpace(),
            LabelName(),
            Optional(WhiteSpace()),
            Ch(';'),
            push(program.gotoLabel((String) pop())),
            drop()
        );
    }

    public Rule Label() {
        return Sequence(
            LabelName(),
            Optional(WhiteSpace()),
            Ch(':'),
            push(program.label((String) pop())),
            drop()
        );
    }

    public Rule Println() {
        return Sequence(
            String("println"),
            Optional(WhiteSpace()),
            Ch('('),
            Optional(WhiteSpace()),
            VarName(),
            Optional(WhiteSpace()),
            Ch(')'),
            Optional(WhiteSpace()),
            Ch(';'),
            push(program.println((String) ((Value) pop()).value)),
            drop()
        );
    }

    public Rule Assignment() {
        return Sequence(
            VarName(),
            Optional(WhiteSpace()),
            VarValue(),
            push(program.assignment((Value) pop(), (String) ((Value) pop()).value)),
            drop()
        );
    }

    public Rule VarName() {
        return Sequence(Name(), push(new Value(ValueType.VAR_NAME, pop())));
    }

    public Rule Name() {
        return Sequence(
            Sequence(
                FirstOf(CharRange('a', 'z'), CharRange('A', 'Z')),
                ZeroOrMore(FirstOf(CharRange('a', 'z'), CharRange('A', 'Z'), CharRange('0', '9')))
            ),
            push(match())
        );
    }

    public Rule LabelName() {
        return Sequence(TestNot(String("goto")), Name());
    }

    public Rule VarValue() {
        return FirstOf(
            Sequence(Ch(';'), push(null)),
            Sequence(Ch('='), Optional(WhiteSpace()),
                FirstOf(VarName(), StringLiteral(), IntegerLiteral()),
                Optional(WhiteSpace()), Ch(';')
            )
        );
    }

    public Rule StringLiteral() {
        return Sequence('"', Sequence(ZeroOrMore(NoneOf("\"")),
            push(new Value(ValueType.STRING, match()))), '"');
    }

    public Rule IntegerLiteral() {
        return Sequence(OneOrMore(CharRange('0', '9')),
            push(new Value(ValueType.INTEGER, Integer.parseInt(match()))));
    }

    public Rule WhiteSpace() {
        return OneOrMore(AnyOf(" \n\t"));
    }
}
