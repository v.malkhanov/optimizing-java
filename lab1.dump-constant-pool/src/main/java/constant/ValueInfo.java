package constant;

public class ValueInfo<T> extends ClassFileCpInfo {
    private final T value;

    public ValueInfo(final Tag tag, final T value) {
        super(tag);
        this.value = value;
    }

    @Override
    public String getInfo() {
        return value.toString();
    }
}
