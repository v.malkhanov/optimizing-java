import org.parboiled.Parboiled;
import org.parboiled.parserunners.RecoveringParseRunner;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;

public class App {
    public static void main(String[] args) {
        if (args.length < 1) {
            return;
        }
        String filePath = args[0];
        try (FileInputStream in = new FileInputStream(filePath)) {
            Parser parser = Parboiled.createParser(Parser.class);
            parser.program = new Program(Path.of(filePath));
            new RecoveringParseRunner<Program>(parser.Program())
                .run(new String(in.readAllBytes()));
            parser.program.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
