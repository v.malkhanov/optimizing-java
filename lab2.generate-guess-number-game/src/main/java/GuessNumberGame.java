import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Random;
import java.util.Scanner;

public class GuessNumberGame implements Opcodes {
    private final ClassWriter classWriter = new ClassWriter(0);

    public byte[] generate() {
        classWriter.visit(V11, ACC_PUBLIC + ACC_SUPER, "GuessNumber",
                null, AsmUtils.absoluteName(Object.class), null);

        classWriter.visitField(ACC_PRIVATE + ACC_FINAL, "random",
                AsmUtils.asmName(Random.class), null, null).visitEnd();

        this.addInit();
        this.addMethodStart();
        this.addMain();

        classWriter.visitEnd();
        return classWriter.toByteArray();
    }

    private void println(MethodVisitor mv, String message) {
        mv.visitFieldInsn(GETSTATIC, AsmUtils.absoluteName(System.class), "out", AsmUtils.asmName(PrintStream.class));
        mv.visitLdcInsn(message);
        mv.visitMethodInsn(INVOKEVIRTUAL, AsmUtils.absoluteName(PrintStream.class),
                "println", String.format("(%s)V", AsmUtils.asmName(String.class)), false);
    }

    private void addMain() {
        MethodVisitor mv = classWriter.visitMethod(ACC_PUBLIC + ACC_STATIC, "main",
                String.format("([%s)V", AsmUtils.asmName(String.class)), null, null);
        mv.visitCode();
        mv.visitTypeInsn(NEW, "GuessNumber");
        mv.visitInsn(DUP);
        mv.visitMethodInsn(INVOKESPECIAL, "GuessNumber", "<init>", "()V", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "GuessNumber", "start", "()V", false);
        mv.visitInsn(RETURN);
        mv.visitMaxs(2, 1);
        mv.visitEnd();
    }

    private void addInit() {
        MethodVisitor mv = classWriter.visitMethod(ACC_PUBLIC, "<init>",
                "()V", null, null);
        mv.visitCode();
        mv.visitVarInsn(ALOAD, 0);
        mv.visitMethodInsn(INVOKESPECIAL, AsmUtils.absoluteName(Object.class), "<init>", "()V", false);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitTypeInsn(NEW, AsmUtils.absoluteName(Random.class));
        mv.visitInsn(DUP);
        mv.visitMethodInsn(INVOKESPECIAL, AsmUtils.absoluteName(Random.class), "<init>", "()V", false);
        mv.visitFieldInsn(PUTFIELD, "GuessNumber", "random", AsmUtils.asmName(Random.class));
        mv.visitInsn(RETURN);
        mv.visitMaxs(3, 1);
        mv.visitEnd();
    }

    private void addMethodStart() {
        MethodVisitor mv = classWriter.visitMethod(ACC_PUBLIC, "start", "()V", null, null);
        mv.visitCode();
        mv.visitVarInsn(ALOAD, 0);

        //int number = this.random.nextInt(100);
        mv.visitFieldInsn(GETFIELD, "GuessNumber", "random", AsmUtils.asmName(Random.class));
        mv.visitIntInsn(BIPUSH, 100);
        mv.visitMethodInsn(INVOKEVIRTUAL, AsmUtils.absoluteName(Random.class), "nextInt", "(I)I", false);
        mv.visitVarInsn(ISTORE, 1);

        //Scanner scanner = new Scanner(System.in);
        mv.visitTypeInsn(NEW, AsmUtils.absoluteName(Scanner.class));
        mv.visitInsn(DUP);
        mv.visitFieldInsn(GETSTATIC, AsmUtils.absoluteName(System.class), "in", AsmUtils.asmName(InputStream.class));
        mv.visitMethodInsn(INVOKESPECIAL, AsmUtils.absoluteName(Scanner.class), "<init>",
                String.format("(%s)V", AsmUtils.asmName(InputStream.class)), false);
        mv.visitVarInsn(ASTORE, 2);

        this.println(mv, "I've thought a number, try to guess!");

        Label loop = new Label();
        Label equal = new Label();
        Label lowerEqual = new Label();

        mv.visitLabel(loop);
        mv.visitFrame(F_APPEND, 2, new Object[]{INTEGER, AsmUtils.absoluteName(Scanner.class)}, 0, null);

        //int inputNumber = scanner.nextInt();
        mv.visitVarInsn(ALOAD, 2);
        mv.visitMethodInsn(INVOKEVIRTUAL, AsmUtils.absoluteName(Scanner.class), "nextInt", "()I", false);
        mv.visitVarInsn(ISTORE, 3);

        //if inputNumber < number
        mv.visitVarInsn(ILOAD, 3);
        mv.visitVarInsn(ILOAD, 1);
        mv.visitJumpInsn(IF_ICMPGE, lowerEqual);
        this.println(mv, "Greater");
        mv.visitJumpInsn(GOTO, loop);

        mv.visitLabel(lowerEqual);
        mv.visitFrame(F_APPEND, 1, new Object[]{INTEGER}, 0, null);

        //if inputNumber > number
        mv.visitVarInsn(ILOAD, 3);
        mv.visitVarInsn(ILOAD, 1);
        mv.visitJumpInsn(IF_ICMPEQ, equal);
        this.println(mv, "Lower");
        mv.visitJumpInsn(GOTO, loop);

        mv.visitLabel(equal);
        mv.visitFrame(F_SAME, 0, null, 0, null);

        //if inputNumber == number
        this.println(mv, "Exactly! Good bye!");

        //scanner.close()
        mv.visitVarInsn(ALOAD, 2);
        mv.visitMethodInsn(INVOKEVIRTUAL, AsmUtils.absoluteName(Scanner.class), "close", "()V", false);

        mv.visitInsn(RETURN);
        mv.visitMaxs(3, 4);
        mv.visitEnd();
    }
}
