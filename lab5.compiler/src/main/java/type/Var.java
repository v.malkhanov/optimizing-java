package type;

public class Var {
    private static int ID_COUNTER = 1;

    public int id;
    public ValueType type;

    public Var(ValueType type) {
        this.id = ID_COUNTER++;
        this.type = type;
    }
}
