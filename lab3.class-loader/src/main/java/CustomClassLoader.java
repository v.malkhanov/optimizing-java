import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;

public class CustomClassLoader extends ClassLoader {
    public Class<?> defineClass(byte[] bytes) {
        return super.defineClass(null, bytes, 0, bytes.length);
    }

    public static void execute(Path path, CustomClassLoader classLoader) {
        try (InputStream in = Files.newInputStream(path)) {
            Class<?> newClass = classLoader.defineClass(in.readAllBytes());
            Method method = newClass.getDeclaredMethod("getSecurityMessage");
            Object instance = newClass.getDeclaredConstructor().newInstance();
            method.setAccessible(true);
            System.out.println(newClass.getCanonicalName() + ": " + method.invoke(instance));
        } catch (NoSuchMethodException ignored) {
        } catch (IOException | IllegalAccessException | InstantiationException | InvocationTargetException ex) {
            ex.printStackTrace();
        }
    }
}
