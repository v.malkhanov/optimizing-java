import net.bytebuddy.agent.ByteBuddyAgent;
import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.matcher.ElementMatchers;

import java.lang.instrument.Instrumentation;

public class Agent {

    public static void premain(String args, Instrumentation instrumentation) {
        new AgentBuilder.Default()
            .type(ElementMatchers.nameEndsWith("TransactionProcessor"))
            .transform((builder, typeDescription, classLoader, javaModule) ->
                builder.visit(Advice.to(TimerAdvice.class).on(ElementMatchers.nameEndsWith("processTransaction")))
            )
            .installOn(instrumentation);

        new AgentBuilder.Default()
            .type(ElementMatchers.nameEndsWith("TransactionProcessor"))
            .transform((builder, typeDescription, classLoader, javaModule) ->
                builder.visit(Advice.to(MainAdvice.class).on(ElementMatchers.nameEndsWith("main")))
            )
            .installOn(instrumentation);

        ByteBuddyAgent.install();
    }
}
